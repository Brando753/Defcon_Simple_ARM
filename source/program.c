#include "program.h"

extern volatile bool rgb_switched;
extern volatile uint32_t ms_counter;
extern struct AES_ctx ctx;
extern uint32_t aes_buffer[4];

/**
* Check if this is the first run of this program, if so clear all
* LED's to 0x0000 and then reset the first run flag so this is skipped
* in future runs.
*/
void first_run(void) {
  if (rgb_switched == true) {
    clear_RGB_led();
    rgb_switched = false;
  }
}

uint32_t intensity(void) {
  static uint32_t intensity = 0;
  static int modifier = 1;
  intensity += modifier;
  if(intensity == 0xFF) {
    modifier = -1;
  } else if(intensity == 0x00) {
    modifier = 1;
  }
  return intensity;
}


void program_one(void) {
  first_run();
  uint32_t temp;
  static uint32_t timestamp = 0;
  if(get_delay(timestamp)) {
    temp = intensity();
    RGB_1_RED = temp;
    RGB_2_RED = temp;
    timestamp = set_delay(10);
  }
}

void program_two(void) {
  first_run();
  uint32_t temp;
  static uint32_t timestamp = 0;
  if(get_delay(timestamp)) {
    temp = intensity();
    RGB_1_BLUE = temp;
    RGB_2_BLUE = temp;
    timestamp = set_delay(10);
  }
}

void program_three(void) {
  first_run();
  uint32_t temp;
  static uint32_t timestamp = 0;
  if(get_delay(timestamp)) {
    temp = intensity();
    RGB_1_GREEN = temp;
    RGB_2_GREEN = temp;
    timestamp = set_delay(10);
  }
}

void program_four(void) {
  static uint32_t led = 0;
  static uint32_t timestamp = 0;
  if(get_delay(timestamp)) {
    switch(led) {
      case 0: /* RED LED ON */
      clear_RGB_led();
      RGB_1_RED = 0x80;
      RGB_2_RED = 0x80;
      break;
      case 1: /* BLUE LED ON */
      clear_RGB_led();
      RGB_1_BLUE = 0x40;
      RGB_2_BLUE = 0x40;
      break;
      case 2: /* GREEN LED ON */
      clear_RGB_led();
      RGB_1_GREEN = 0x40;
      RGB_2_GREEN = 0x40;
      break;
      case 3: /* White LED */
      clear_RGB_led();
      RGB_1_RED = 0x80;
      RGB_2_RED = 0x80;
      RGB_1_BLUE = 0x40;
      RGB_2_BLUE = 0x40;
      RGB_1_GREEN = 0x40;
      RGB_2_GREEN = 0x40;
      break;
    }
    timestamp = set_delay(2000);
    led = (led + 1) % 4;
  }
}

void program_five(void) {
  static uint32_t led = 0;
  static uint32_t intensity = 0;
  static uint32_t timestamp = 0;
  if(get_delay(timestamp)) {
    switch(led) {
      case 0: /* RED LED ON */
      clear_RGB_led();
      RGB_1_RED = intensity;
      RGB_2_RED = intensity;
      break;
      case 1: /* BLUE LED ON */
      clear_RGB_led();
      RGB_1_BLUE = intensity;
      RGB_2_BLUE = intensity;
      break;
      case 2: /* GREEN LED ON */
      clear_RGB_led();
      RGB_1_GREEN = intensity;
      RGB_2_GREEN = intensity;
      break;
    }
    timestamp = set_delay(10);
    intensity += 1;
    if (intensity == 0xFF) {
      led = (led + 1) % 3;
      intensity = 0;
    }
  }
}

void program_six(void) {
  first_run();
  static uint8_t r=0, g=0, b=0;
  static uint32_t timestamp = 0;
  if (get_delay(timestamp)) {
    RGB_1_RED = r;
    RGB_2_RED = r;
    RGB_1_BLUE = b;
    RGB_2_BLUE = b;
    RGB_1_GREEN = g;
    RGB_2_GREEN = g;
    if (r == 0xFF) {
      r = 0;
      if (b == 0xFF) {
        b = 0;
        g += 1;
      } else {
        b += 1;
      }
    }else {
      r += 1;
    }

    timestamp = set_delay(1);
  }
}

uint8_t get_index(void) {
  static int counter = 0;
  static uint32_t ECB_counter = 1;
  int i;
  if (counter == 16) {
    for(i=0;i<4;i++) {
      aes_buffer[i] = 0;
    }
    aes_buffer[0] = ECB_counter;
    ECB_counter++;
    AES_ECB_encrypt(&ctx, (uint8_t*)aes_buffer);
    counter = 0;
  }
  counter++;
  return (((uint8_t*) aes_buffer)[counter-1]) % 0xA0;
}

void program_seven(void) {
  first_run();
  static uint32_t timestamp = 0;
  if(get_delay(timestamp)) {
    RGB_1_RED = get_index();
    RGB_2_RED = get_index();
    RGB_1_GREEN = get_index();
    RGB_2_GREEN = get_index();
    RGB_1_BLUE = get_index();
    RGB_2_BLUE = get_index();
    timestamp = set_delay(200);
  }
}

void program_eight(void) {
  while(GPIO_PinInGet(M_BUTTON_PORT, M_BUTTON_PIN) == 0)
  clear_led();
  GPIO_EM4EnablePinWakeup(0x10, 0x00);
  EMU_EnterEM4();
}

void led_mode_1(void) {
  static uint8_t led = 0;
  uint8_t *leds[] = {&LED1, &LED2, &LED3, &LED4, &LED5};
  static uint32_t timestamp = 0;
  if(get_delay(timestamp)) {
    if (led % 5 == 0) {
      *leds[4] = 0x00;
      *leds[0] = 0xA0;
      led = 1;
    }
    else {
      *leds[led-1] = 0x00;
      *leds[led] = 0xA0;
      led++;
    }
    timestamp = set_delay(300);
  }
}

void led_mode_2(void) {
  LED1 = 0xA0;
  LED2 = 0xA0;
  LED3 = 0xA0;
  LED4 = 0xA0;
  LED5 = 0xA0;
}

void led_mode_3(void) {
  LED1 = 0xFF;
  LED2 = 0xFF;
  LED3 = 0xFF;
  LED4 = 0xFF;
  LED5 = 0xFF;
}

void led_mode_4(void) {
  LED1 = 0x0A;
  LED2 = 0x0A;
  LED3 = 0x0A;
  LED4 = 0x0A;
  LED5 = 0x0A;
}

void led_mode_5(void) {
  LED1 = 0x00;
  LED2 = 0x00;
  LED3 = 0x00;
  LED4 = 0x00;
  LED5 = 0x00;
}
