#ifndef SFT_PWM_H
#define SFT_PWM_H

#include "badge.h"

#define RGB_1_RED pwm_led_buf[0]
#define RGB_1_BLUE pwm_led_buf[1]
#define RGB_1_GREEN pwm_led_buf[2]
#define LED1 pwm_led_buf[3]
#define LED2 pwm_led_buf[4]
#define LED3 pwm_led_buf[5]
#define LED4 pwm_led_buf[6]
#define LED5 pwm_led_buf[7]

extern uint8_t pwm_led_buf[8];


void enable_soft_pwm(void);

#endif
