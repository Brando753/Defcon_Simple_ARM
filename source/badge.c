#include "badge.h"

extern volatile uint32_t ms_counter;

/* Setup an async delay */
uint32_t set_delay(uint32_t delay_ms) {
  uint32_t timestamp;
  timestamp = ms_counter;
  timestamp = timestamp + delay_ms;
  return timestamp;
}

/* Takes a timestamp returned from set_delay */
bool get_delay(uint32_t timestamp) {
  if (((int64_t)ms_counter - timestamp) >= 0) {
    return true;
  }
  return false;
}

void enable_badge(void) {
  CHIP_Init();
  CMU_HFRCOBandSet(cmuHFRCOBand_1MHz);    // Set HF oscillator to 1MHz and use as system source
  CMU_ClockEnable(cmuClock_HFPER, true);
}

void enable_button(void) {
  CMU_ClockEnable(cmuClock_GPIO, true);   // enable GPIO peripheral clock
  GPIO_PinModeSet(M_BUTTON_PORT, M_BUTTON_PIN, gpioModeInput, 1); // set M_BUTTON pin as input
  GPIO_ExtIntConfig(M_BUTTON_PORT, M_BUTTON_PIN, M_BUTTON_PIN, false, true, true);
  NVIC_EnableIRQ(GPIO_EVEN_IRQn);
}

void disable_debug(void) {
  /* Mark first boot mode */
  SET_LOW(LED1_PORT, LED1_PIN);
  SET_LOW(LED3_PORT, LED3_PIN);
  SET_LOW(LED5_PORT, LED5_PIN);

  if(GPIO_EM4GetPinWakeupCause() != 0x10) {

  for(;;) {
    if(ms_counter > 3000)  {/* wait 3 seconds */
      break;
    }
  }
}

  GPIO_DbgSWDIOEnable(false);
  GPIO_DbgSWDClkEnable(false);

  SET_HIGH(LED1_PORT, LED1_PIN);
  SET_HIGH(LED3_PORT, LED3_PIN);
  SET_HIGH(LED5_PORT, LED5_PIN);

}

/**
 * Shutoff the SPI flash chip AT25SF041 that was thrown on the board just
 * to increase the badge cost and for no other reason whatsoever.
 * Move along nothing to see here.
 *
 * Places external flash in Deep Power-Down Mode to save power
 * precious precious microAmps.
 */
void disable_flash_chip(void) {
  uint8_t rx_buffer[RX_BUFFER_SIZE] __attribute__((unused));
  CMU_ClockEnable(cmuClock_CORELE, true);                  // enable the Low Energy Peripheral Interface clock
  CMU_ClockEnable(cmuClock_USART1, true);                  // enable USART1 peripheral clock

  /* Configure GPIO */
  GPIO_PinModeSet(SPI_MISO_PORT, SPI_MISO_PIN, gpioModeInput, 0);       // configure MISO pin as input, no filter
  GPIO_PinModeSet(SPI_MOSI_PORT, SPI_MOSI_PIN, gpioModePushPull, 1);    // configure MOSI pin as output, initialize high
  GPIO_PinModeSet(SPI_CS_PORT, SPI_CS_PIN, gpioModePushPull, 1);      // configure CS pin as output, initialize high
  GPIO_PinModeSet(SPI_SCLK_PORT, SPI_SCLK_PIN, gpioModePushPull, 0);    // configure SCLK pin as output, initialize low

  /* Configure SPI Port (USART1 in sync mode) */
  USART_InitSync_TypeDef spi_init = {
    .enable = usartEnable,        // enable bidirectional data (TX and RX)
    .refFreq = 0,                 // measure source clock
    .baudrate = 12000000,         // 12Mbps is max data rate with 24MHz clock source
    .databits = usartDatabits8,   // 8 data bits per frame
    .master = true,               // configure as SPI master
    .msbf = true,                 // transmit msb first (requirement of W25X40CL)
    .clockMode = usartClockMode0, // clock idles low, data setup on rising edge, sampled on falling edge
  };
  USART_IntClear(USART1, 0x1FF9);                 // clear interrupt flags (optional)
  USART_InitSync(USART1, &spi_init);

  USART1->ROUTE = (3 << 8) | ( 1 << 3) | (3 << 0); // use location #3, enable TX/RX/CLK pins

  GPIO_PinOutClear(SPI_CS_PORT, SPI_CS_PIN); /* Enable Chip Select */
  rx_buffer[0] = USART_SpiTransfer(USART1, 0xB9); /* Transmit shutdown command */
  GPIO_PinOutSet(SPI_CS_PORT, SPI_CS_PIN); /* Disable Chip Select */

  /* Disable SPI driver and SPI clock */
  USART_Enable(USART1, usartDisable);
  CMU_ClockEnable(cmuClock_USART1, false);
}

void enable_pwm(void) {
  CMU_ClockEnable(cmuClock_TIMER1, true); // enable TIMER3 peripheral clock

  enable_led();

  // Setup Timer Channel Configuration for PWM
  TIMER_InitCC_TypeDef timerCCInit = {
    .eventCtrl  = timerEventEveryEdge,    // this value will be ignored since we aren't using input capture
    .edge       = timerEdgeNone,          // this value will be ignored since we aren't using input capture
    .prsSel     = timerPRSSELCh1,         // this value will be ignored since we aren't using PRS
    .cufoa      = timerOutputActionNone,  // no action on underflow (up-count mode)
    .cofoa      = timerOutputActionSet,   // on overflow, we want the output to go high, but in PWM mode this should happen automatically
    .cmoa       = timerOutputActionClear, // on compare match, we want output to clear, but in PWM mode this should happen automatically
    .mode       = timerCCModePWM,         // set timer channel to run in PWM mode
    .filter     = false,                  // not using input, so don't need a filter
    .prsInput   = false,                  // not using PRS
    .coist      = false,                  // initial state for PWM is high when timer is enabled
    .outInvert  = true,                  // non-inverted output
  };

  // Setup Timer Configuration for PWM
  TIMER_Init_TypeDef timerPWMInit =
  {
    .enable     = true,                 // start timer upon configuration
    .debugRun   = true,                 // run timer in debug mode
    .prescale   = timerPrescale1,       // set prescaler to /1
    .clkSel     = timerClkSelHFPerClk,  // set clock source as HFPERCLK
    .fallAction = timerInputActionNone, // no action from inputs
    .riseAction = timerInputActionNone, // no action from inputs
    .mode       = timerModeUp,          // use up-count mode
    .dmaClrAct  = false,                // not using DMA
    .quadModeX4 = false,                // not using Quad Dec. mode
    .oneShot    = false,                // not using one shot mode
    .sync       = false,                // not syncronizing timer3 with other timers
  };

  TIMER_TopSet(TIMER1, TOP_VAL_PWM);           // PWM period will be 1ms = 1kHz freq

  TIMER_CounterSet(TIMER1, 0);                 // start counter at 0 (up-count mode)

  /* Shut off the LED's during init phase */
  TIMER_CompareSet(TIMER1, 0, 0x00);    // set CC0 compare value (0% duty)
  TIMER_CompareSet(TIMER1, 1, 0x00);
  TIMER_CompareSet(TIMER1, 2, 0x00);

  /* Initialize the timer channels */
  TIMER_InitCC(TIMER1, 0, &timerCCInit);
  TIMER_InitCC(TIMER1, 1, &timerCCInit);
  TIMER_InitCC(TIMER1, 2, &timerCCInit);

  TIMER1->ROUTE = (3 << 16) | (1 << 0) | (1 << 1) | (1 << 2);         // connect PWM output (timer1, channel 0) to PE10 (LED0). See EFM32ZG222 datasheet for details.

  TIMER_Init(TIMER1, &timerPWMInit);           // apply PWM configuration to timer1
  enable_soft_pwm();
}

void enable_led(void) {
  CMU_ClockEnable(cmuClock_GPIO, true);   // enable GPIO peripheral clock
  GPIO_DriveModeSet(LED1_PORT, gpioDriveModeHigh);
  GPIO_DriveModeSet(LED2_PORT, gpioDriveModeHigh);
  GPIO_DriveModeSet(LED3_PORT, gpioDriveModeHigh);
  GPIO_DriveModeSet(LED4_PORT, gpioDriveModeHigh);
  GPIO_DriveModeSet(LED5_PORT, gpioDriveModeHigh);
  GPIO_DriveModeSet(RGB_1_RED_PORT, gpioDriveModeHigh);
  GPIO_DriveModeSet(RGB_1_BLUE_PORT, gpioDriveModeHigh);
  GPIO_DriveModeSet(RGB_1_GREEN_PORT, gpioDriveModeHigh);
  GPIO_DriveModeSet(RGB_2_RED_PORT, gpioDriveModeHigh);
  GPIO_DriveModeSet(RGB_2_BLUE_PORT, gpioDriveModeHigh);
  GPIO_DriveModeSet(RGB_2_GREEN_PORT, gpioDriveModeHigh);
  GPIO_PinModeSet(LED1_PORT, LED1_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(LED2_PORT, LED2_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(LED3_PORT, LED3_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(LED4_PORT, LED4_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(LED5_PORT, LED5_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(RGB_1_RED_PORT, RGB_1_RED_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(RGB_1_GREEN_PORT, RGB_1_GREEN_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(RGB_1_BLUE_PORT, RGB_1_BLUE_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(RGB_2_RED_PORT, RGB_2_RED_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(RGB_2_GREEN_PORT, RGB_2_GREEN_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(RGB_2_BLUE_PORT, RGB_2_BLUE_PIN, gpioModePushPull, 1);
}

void disable_button(void) {
  CMU_ClockEnable(cmuClock_GPIO, false);   // enable GPIO peripheral clock
  NVIC_DisableIRQ(GPIO_EVEN_IRQn);
}

void disable_pwm(void) {

}

void clear_led(void) {
  clear_top_led();
  clear_RGB_led();
}
void clear_top_led(void) {
  GPIO_PinOutSet(LED1_PORT, LED1_PIN);
  GPIO_PinOutSet(LED2_PORT, LED2_PIN);
  GPIO_PinOutSet(LED3_PORT, LED3_PIN);
  GPIO_PinOutSet(LED4_PORT, LED4_PIN);
  GPIO_PinOutSet(LED5_PORT, LED5_PIN);
  LED1 = 0x00;
  LED2 = 0x00;
  LED3 = 0x00;
  LED4 = 0x00;
  LED5 = 0x00;
}

void clear_RGB_led(void) {
  GPIO_PinOutSet(RGB_1_RED_PORT, RGB_1_RED_PIN);
  GPIO_PinOutSet(RGB_1_GREEN_PORT, RGB_1_GREEN_PIN);
  GPIO_PinOutSet(RGB_1_BLUE_PORT, RGB_1_BLUE_PIN);
  GPIO_PinOutSet(RGB_2_RED_PORT, RGB_2_RED_PIN);
  GPIO_PinOutSet(RGB_2_GREEN_PORT, RGB_2_GREEN_PIN);
  GPIO_PinOutSet(RGB_2_BLUE_PORT, RGB_2_BLUE_PIN);
  RGB_1_RED = 0x00;
  RGB_1_GREEN = 0x00;
  RGB_1_BLUE = 0x00;
  RGB_2_RED = 0x00;
  RGB_2_GREEN = 0x00;
  RGB_2_BLUE = 0x00;
}
