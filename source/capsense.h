#ifndef CAPSENSE_H
#define CAPSENSE_H

#include "em_acmp.h"
#include "em_prs.h"
#include "em_pcnt.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_rtc.h"

/* Define Capacitive push button port and pin */
#define C_BUTTON_PORT gpioPortC
#define C_BUTTON_PIN 0

 #define ACMP_CAPSENSE                           ACMP0
 #define ACMP_CAPSENSE_CLKEN                     CMU_HFPERCLKEN0_ACMP0
 #define PRS_CH_CTRL_SOURCESEL_ACMP_CAPSENSE     PRS_CH_CTRL_SOURCESEL_ACMP0
 #define PRS_CH_CTRL_SIGSEL_ACMPOUT_CAPSENSE     PRS_CH_CTRL_SIGSEL_ACMP0OUT

 #define ACMP_CHANNELS           8             /**< Number of channels for the Analog Comparator */

 #define BUTTON0_CHANNEL         0             /**< Button 0 channel */
 #define BUTTON1_CHANNEL         0             /**< Button 1 channel */

 #define CAPSENSE_CH_IN_USE  { true, false, false, false , false, false, false, false }
 #define LED_PORT         gpioPortB
 #define LED0_PIN         8


 /* CMU defines */
 #define ACMP_CAPSENSE_CMUCLOCK  cmuClock_ACMP0

 /* PCNT defines */
 #define PCNT_PRS_CH     0
 #define PCNT_TOP        0xFFFF
 #define PCNT_MAX        PCNT_TOP + 1

 /* ACMP parameters, items are device dependent */
 #define ACMP_FULLBIAS   false
 #define ACMP_HALFBIAS   false
 #define ACMP_BIASPROG   7
 #define ACMP_WARMTIME   acmpWarmTime256 /* Clock dependent */
 #define ACMP_HYS        acmpHysteresisLevel5
 #define ACMP_RESISTOR   acmpResistor3
 #define ACMP_VDD        0x3D

 /* Capsense parameters */
 #define SAMPLE_TIME     10      /* Time = n x 1/32768s */
 #define SCAN_FREQUENCY  2047    /* Frequency = 32768/(n+1)Hz */
 #define THRESHOLD_SHIFT 2       /* Divided by 2^n */


/**
 * Function declerations.
 */
void enable_capSense(void);
void disable_capSense(void);

void CAPSENSE_getReady(void);
bool CAPSENSE_getPressed(uint8_t channel);

#endif
