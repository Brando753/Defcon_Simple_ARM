#include "capsense.h"

/* Previous PCNT value */
static uint16_t oldPcnt;
/* Current PCNT value */
static volatile uint16_t newPcnt;
/* The current sensing channel. */
static uint8_t currentChannel;
/* Flag for measurement completion. */
static volatile bool measurementComplete;
/* Store the latest read values from the ACMP */
static uint32_t channelValues[ACMP_CHANNELS];
/* Stores the maximum values seen by a channel */
static uint32_t channelMaxValues[ACMP_CHANNELS];
/* Define which capsense channels are in use */
static const bool channelsInUse[ACMP_CHANNELS] = CAPSENSE_CH_IN_USE;


/**************************************************************************//**
 * @brief RTC_IRQHandler
 *****************************************************************************/
void RTC_IRQHandler( void )
{
  uint32_t chnTemp;

  if (RTC->IEN & RTC_IEN_COMP1)
  {
    /* Get count on current scanning channel */
    newPcnt = PCNT0->CNT;

    for (chnTemp = currentChannel+1; chnTemp < ACMP_CHANNELS; chnTemp++)
    {
      /* If this channel is not in use, skip to the next one */
      if (!channelsInUse[chnTemp])
      {
        continue;
      }
      /* Setup sample time and enable next channel if not end */
      ACMP_CapsenseChannelSet(ACMP_CAPSENSE, (ACMP_Channel_TypeDef)chnTemp);
      RTC->COMP1 = RTC->CNT + SAMPLE_TIME;
      break;
    }

    if (chnTemp == ACMP_CHANNELS)
    {
      /* End of scanning */
      ACMP_Disable(ACMP_CAPSENSE);
      RTC->IEN = RTC_IEN_COMP0;
      measurementComplete = true;
    }
    RTC_IntClear(RTC_IFC_COMP1);

    /* Save current count value */
    if (newPcnt > oldPcnt)
    {
      channelValues[currentChannel] = newPcnt - oldPcnt;
    }
    else
    {
      channelValues[currentChannel] = PCNT_MAX - oldPcnt + newPcnt;
    }
    oldPcnt = newPcnt;

    /* Update maximum value */
    if (channelValues[currentChannel] > channelMaxValues[currentChannel])
    {
      channelMaxValues[currentChannel] = channelValues[currentChannel];
    }
    currentChannel = chnTemp;
  }
  else
  {
    RTC_IntClear(RTC_IFC_COMP0 + RTC_IFC_COMP1);

    for (currentChannel = 0; currentChannel < ACMP_CHANNELS; currentChannel++)
    {
      /* If this channel is not in use, skip to the next one */
      if (!channelsInUse[currentChannel])
      {
        continue;
      }
      /* Setup sample time and enable first channel */
      ACMP_CapsenseChannelSet(ACMP_CAPSENSE, (ACMP_Channel_TypeDef)currentChannel);
      ACMP_Enable(ACMP_CAPSENSE);
      break;
    }
    RTC->COMP1 = RTC->CNT + SAMPLE_TIME;
    RTC->IEN = RTC_IEN_COMP0 + RTC_IEN_COMP1;
  }
}

/**************************************************************************//**
 * @brief  Enable clocks for all the peripherals to be used
 *****************************************************************************/
void setupCMU(void)
{
  /* Use 14 MHz HFRCO as core clock */
  CMU_HFRCOBandSet(cmuHFRCOBand_14MHz);

  /* ACMP */
  CMU_ClockEnable(ACMP_CAPSENSE_CMUCLOCK, true);

  /* PRS */
  CMU_ClockEnable(cmuClock_PRS, true);

  /* RTC */
  CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFRCO);
  CMU_ClockEnable(cmuClock_CORELE, true);
  CMU_ClockEnable(cmuClock_RTC, true);

  /* PCNT */
  CMU_ClockEnable(cmuClock_PCNT0, true);
}

/**************************************************************************//**
 * @brief  Sets up the PRS
 *****************************************************************************/
void setupPRS(void)
{
  /* PRS channel configuration. */
  PRS_SourceAsyncSignalSet(PCNT_PRS_CH, PRS_CH_CTRL_SOURCESEL_ACMP_CAPSENSE, PRS_CH_CTRL_SIGSEL_ACMPOUT_CAPSENSE);
}

/**************************************************************************//**
 * @brief  Sets up the PCNT
 *****************************************************************************/
void setupPCNT(void)
{
  /* PCNT configuration table. */
  PCNT_Init_TypeDef initPCNT =
  {
    .mode        = pcntModeExtSingle,   /* External, single mode. */
    .counter     = 0,                   /* Counter value has been initialized to 0. */
    .top         = PCNT_TOP,            /* Counter top value. */
    .negEdge     = false,               /* Use positive edge. */
    .countDown   = false,               /* Up-counting. */
    .filter      = false,               /* Filter disabled. */
    .hyst        = false,               /* Hysteresis disabled. */
    .s1CntDir    = false,               /* Counter direction is given by S1. */
    .cntEvent    = pcntCntEventUp,      /* Regular counter counts up on upcount events. */
    .auxCntEvent = pcntCntEventNone,    /* Auxiliary counter doesn't respond to events. */
    .s0PRS       = pcntPRSCh0,          /* PRS channel 0 selected as S0IN. */
    .s1PRS       = pcntPRSCh1           /* PRS channel 1 selected as S1IN. */
  };

  /* Initialize PCNT. */
  PCNT_Init(PCNT0, &initPCNT);

  /* Enable PRS input S0 in PCNT. */
  PCNT_PRSInputEnable(PCNT0, pcntPRSInputS0, true);

  /* Sync up to end PCNT initialization */
  while(PCNT0->SYNCBUSY)
  {
    PRS_LevelSet(PRS_SWLEVEL_CH0LEVEL, PRS_SWLEVEL_CH0LEVEL);
    PRS_LevelSet(~PRS_SWLEVEL_CH0LEVEL, PRS_SWLEVEL_CH0LEVEL);
  }
}

/**************************************************************************//**
 * @brief  Sets up the RTC
 *****************************************************************************/
void setupRTC( void )
{
  RTC_Init_TypeDef rtcInit = RTC_INIT_DEFAULT;

  rtcInit.enable = false;
  RTC_Init(&rtcInit);

  /* Set RTC compare value */
  RTC_CompareSet(0, SCAN_FREQUENCY);

  /* Enable RTC interrupt from COMP0 */
  RTC_IntEnable(RTC_IEN_COMP0);
  /* Enable RTC interrupt vector in NVIC */
  NVIC_ClearPendingIRQ(RTC_IRQn);
  NVIC_EnableIRQ(RTC_IRQn);
}

/**************************************************************************//**
 * @brief Get the state of ACMP, return until ACMP is ready
 *****************************************************************************/
void CAPSENSE_getReady(void)
{
  if (RTC->IEN & RTC_IEN_COMP1)
  {
    while (!(ACMP_CAPSENSE->STATUS & ACMP_STATUS_ACMPACT))
      ;
  }
}

/**************************************************************************//**
 * @brief Get the state of the Gecko Button
 * @param channel The channel.
 * @return true if the button is "pressed"
 *         false otherwise.
 *****************************************************************************/
bool CAPSENSE_getPressed(uint8_t channel)
{
  uint32_t threshold;
  /* Hold until last measurement completes */
  CAPSENSE_getReady();
  /* Threshold = channel maximum - channel maximum/2^n */
  threshold = channelMaxValues[channel] - (channelMaxValues[channel] >> THRESHOLD_SHIFT);

  if (channelValues[channel] < threshold)
  {
    return true;
  }
  return false;
}

void enable_capSense(void) {
  /* Use the default STK capacative sensing setup */
  ACMP_CapsenseInit_TypeDef capsenseInit =
  {                                                     \
    ACMP_FULLBIAS,      /* FullBias */                  \
    ACMP_HALFBIAS,      /* HalfBias */                  \
    ACMP_BIASPROG,      /* BiasProg */                  \
    ACMP_WARMTIME,      /* Warmup time */               \
    ACMP_HYS,           /* Hysteresis level */          \
    ACMP_RESISTOR,      /* Internal resistor value */   \
    false,              /* low power reference */       \
    ACMP_VDD,           /* VDD level */                 \
    false               /* Enable after init. */        \
  };

  setupCMU();
  setupPRS();
  setupPCNT();
  setupRTC();

  /* Set up ACMP in capsense mode, start scanning */
  ACMP_CapsenseInit(ACMP_CAPSENSE, &capsenseInit);
  RTC_Enable(true);
}

void disable_capSense(void) {
  RTC_Enable(false);
  /* ACMP */
  CMU_ClockEnable(ACMP_CAPSENSE_CMUCLOCK, false);
  /* PRS */
  CMU_ClockEnable(cmuClock_PRS, false);
  /* RTC */
  CMU_ClockEnable(cmuClock_RTC, false);
  /* PCNT */
  CMU_ClockEnable(cmuClock_PCNT0, false);
}
