#include "soft_pwm.h"

uint8_t pwm_led_buf[8] = {0x00};
uint8_t pwm_led[8] = {0x00};
uint8_t pwm_led_pin[] = {
  RGB_1_RED_PIN, RGB_1_BLUE_PIN, RGB_1_GREEN_PIN,
  LED1_PIN, LED2_PIN, LED3_PIN, LED4_PIN, LED5_PIN
};
uint8_t pwm_led_port[] = {
  RGB_1_RED_PORT, RGB_1_BLUE_PORT, RGB_1_GREEN_PORT,
  LED1_PORT, LED2_PORT, LED3_PORT, LED4_PORT, LED5_PORT
};

void TIMER0_IRQHandler(void) {
  TIMER_IntClear(TIMER0, TIMER_IF_OF);
  static volatile uint8_t counter = 0;
  int i;
  /* Copy the buffer after a complete cycle to minimize glitch */
  if (counter == 0x00) {
    for(i=0;i<8;i++) {
      pwm_led[i] = pwm_led_buf[i];
    }
  }
  for(i=0;i<8;i++) {
    if(counter < pwm_led[i]) {
      SET_LOW(pwm_led_port[i], pwm_led_pin[i]);
    }
    else {
      SET_HIGH(pwm_led_port[i], pwm_led_pin[i]);
    }
    counter++;
  }


}

void enable_soft_pwm(void) {
  CMU_ClockEnable(cmuClock_TIMER0, true); // enable TIMER0 peripheral clock
  TIMER_Init_TypeDef timer0_init =
  {
    .enable     = true,                 // start timer upon configuration
    .debugRun   = true,                 // keep timer running even on debug halt
    .prescale   = timerPrescale1,       // use /1 prescaler; TIMER0 clock = HF clock = 1MHz
    .clkSel     = timerClkSelHFPerClk,  // set HF peripheral clock as clock source
    .fallAction = timerInputActionNone, // no action on falling edge
    .riseAction = timerInputActionNone, // no action on rising edge
    .mode       = timerModeUp,          // use up-count mode
    .dmaClrAct  = false,                // not using DMA
    .quadModeX4 = false,                // not using quad decoder
    .oneShot    = false,                // using continuous mode, not one-shot
    .sync       = false,                // not synchronizing with other timers
  };

  TIMER_TopSet(TIMER0, CMU_ClockFreqGet(cmuClock_HFPER) / 10000);
  TIMER_IntEnable(TIMER0, TIMER_IF_OF);
  NVIC_EnableIRQ(TIMER0_IRQn);
  TIMER_Init(TIMER0, &timer0_init);
}
