#include "badge.h"
#include "aes.h"


/**
 * This file contains all the independent programs (modes) that the
 * Llama-badge can enter, these where split into functions to
 * minimize the overhead of the main function.
 */

void program_one(void);
void program_two(void);
void program_three(void);
void program_four(void);
void program_five(void);
void program_six(void);
void program_seven(void);
void program_eight(void);

void led_mode_1(void);
void led_mode_2(void);
void led_mode_3(void);
void led_mode_4(void);
void led_mode_5(void);
