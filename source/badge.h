#ifndef BADGE_H
#define BADGE_H

#include "em_device.h"
#include "em_system.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_timer.h"
#include "em_usart.h"
#include "em_emu.h"
#include "soft_pwm.h"

/* Define reusable Macro for bit maniupulation and pin management */
#define SET_LOW(port, pin) GPIO->P[port].DOUTCLR |= (1<<pin)
#define SET_HIGH(port, pin) GPIO->P[port].DOUTSET |= (1<<pin)
#define TOGGLE_VALUE(port, pin) GPIO->P[port].DOUTTGL |= (1<<pin)

/* Define Defcon 25 Badge Specific Components */
/* Define Blue LED Ports and Pins */
#define LED1_PORT gpioPortB
#define LED1_PIN 13
#define LED2_PORT gpioPortB
#define LED2_PIN 14
#define LED3_PORT gpioPortF
#define LED3_PIN 0
#define LED4_PORT gpioPortE
#define LED4_PIN 12
#define LED5_PORT gpioPortE
#define LED5_PIN 13

/* Define RGB LED Ports and Pins */
#define RGB_1_RED_PIN 0
#define RGB_1_RED_PORT gpioPortA
//#define RGB_1_RED TIMER0->CC[0].CCVB

#define RGB_1_BLUE_PIN 1
#define RGB_1_BLUE_PORT gpioPortC
//#define RGB_1_BLUE TIMER0->CC[2].CCVB

#define RGB_1_GREEN_PIN 1
#define RGB_1_GREEN_PORT gpioPortF
//#define RGB_1_GREEN TIMER0->CC[1].CCVB

#define RGB_2_RED_PIN 7
#define RGB_2_RED_PORT gpioPortB
#define RGB_2_RED TIMER1->CC[0].CCVB

#define RGB_2_BLUE_PIN 11
#define RGB_2_BLUE_PORT gpioPortB
#define RGB_2_BLUE TIMER1->CC[2].CCVB

#define RGB_2_GREEN_PIN 8
#define RGB_2_GREEN_PORT gpioPortB
#define RGB_2_GREEN TIMER1->CC[1].CCVB

/* Define physical push button port and pin */
#define M_BUTTON_PORT gpioPortF
#define M_BUTTON_PIN 2

/* Define SPI related ports and pins */
#define SPI_MISO_PIN  6  // pd6
#define SPI_MISO_PORT gpioPortD
#define SPI_MOSI_PIN  7  // pd7
#define SPI_MOSI_PORT gpioPortD
#define SPI_CS_PIN    14 // pc14
#define SPI_CS_PORT   gpioPortC
#define SPI_SCLK_PIN  15 // pc15
#define SPI_SCLK_PORT gpioPortC

#define RX_BUFFER_SIZE 8


/* Set PWM values for RGB LED's */
/* DONT TOUCH UNLESS YOU KNOW WHAT YOU ARE DOING (That Means You FlyingG0D!)*/
#define TOP_VAL_PWM      0xFF            // sets PWM frequency to 1kHz (1MHz timer clock)
#define UPDATE_PERIOD    250             // update compare value, toggle LED1 every 1/4 second (250ms)
#define INC_VAL          (TOP_VAL_PWM/32) // adjust compare value amount

 /*
 * Functions for enabling or disabling badge features.
 */
void enable_badge(void);
void enable_button(void);
void enable_capSense(void);
void disable_flash_chip(void);
void enable_pwm(void);
void enable_led(void);
void disable_button(void);
void disable_debug(void); /* Call after Systick is enabled */
void disable_capSense(void);
void disable_spi(void);
void disable_pwm(void);
void clear_led(void);
void clear_top_led(void);
void clear_RGB_led(void);
/**
 * Utility functions
 */
 /* Setup an async delay */
 uint32_t set_delay(uint32_t delay_ms);

 /* Takes a timestamp returned from set_delay */
 bool get_delay(uint32_t timestamp);

#endif
