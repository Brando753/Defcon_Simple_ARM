#include "badge.h"
#include "capsense.h"
#include "program.h"
#include "aes.h"

#define RGB_MODES 8
#define LED_MODES 5

volatile uint32_t ms_counter = 0;
volatile uint32_t rgb_mode = 0;
volatile bool rgb_switched = true;
volatile uint32_t led_mode = 0;

struct AES_ctx ctx;
uint8_t key[] = {"Llama-Tipping 28"};
uint32_t aes_buffer[4] = {0};

// Button Interrupt handler
void GPIO_EVEN_IRQHandler(void) {
  GPIO_IntClear(1<<M_BUTTON_PIN);
  rgb_mode = (rgb_mode+1) % RGB_MODES;
  rgb_switched = true;
}

// SysTick TIMER Interrupt executes every ms
void SysTick_Handler(void) {
  ms_counter++; // increment sysTick counter
}


int main(void) {
  bool cap_press = false;
  uint32_t timestamp = 0;
  enable_badge();
  enable_button();
  enable_capSense();
  enable_pwm();
  enable_led();
  disable_flash_chip();

  if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000)) while (1) ;

  disable_debug();
  GPIO->CMD = GPIO_CMD_EM4WUCLR;

  /* Setup AES Buffer */
  AES_init_ctx(&ctx, key);
  AES_ECB_encrypt(&ctx, (uint8_t*)aes_buffer);

  for(;;) {
    if(get_delay(timestamp)) {
      if(CAPSENSE_getPressed(BUTTON0_CHANNEL)) {
        if (cap_press == false) {
          led_mode = (led_mode + 1) % LED_MODES;
          cap_press = true;
        }
      } else if (cap_press == true) {
        cap_press = false;
      }
      timestamp = set_delay(50);
    }

   switch(rgb_mode) {
     case 0:
     program_one();
     break;
     case 1:
     program_two();
     break;
     case 2:
     program_three();
     break;
     case 3:
     program_four();
     break;
     case 4:
     program_five();
     break;
     case 5:
     program_six();
     break;
     case 6:
     program_seven();
     break;
     case 7:
     program_eight();
     break;
   }

   switch(led_mode) {
     case 0:
     led_mode_1();
     break;
     case 1:
     led_mode_2();
     break;
     case 2:
     led_mode_3();
     break;
     case 3:
     led_mode_4();
     break;
     case 4:
     led_mode_5();
     break;
   }

   EMU_EnterEM1(); /* Go to sleep until a timer wakes me up */

  }
  return 0;
}
