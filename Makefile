CC=arm-none-eabi-gcc
SIZE=arm-none-eabi-size
OBJ=arm-none-eabi-objcopy
AR=arm-none-eabi-ar
MICRO=EFM32ZG108F8

# This is debugging with NO Optimizations (Ideal for very small code tests)
#CFLAGS=-ggdb3 -gdwarf-2 -g3 -mcpu=cortex-m0plus -mthumb -std=c99 '-DHAL_CONFIG=1' '-DDEBUG=1' -Wl,-static -ffunction-sections -Wl,--gc-sections -I./EFM_SDK/emlib/inc/ -I./EFM_SDK/EFM32ZG/Include/ -I./EFM_SDK/CMSIS/Include/ -O0 -Wall -fmessage-length=0 --specs=nano.specs -mno-sched-prolog -fno-builtin -ffunction-sections -fdata-sections -MMD -MP

# This is debugging with Non-Crazy Optimizations, will still be mostly
# debuggable (Ideal for most debug tests)
#CFLAGS=-ggdb3 -gdwarf-2 -g3 -mcpu=cortex-m0plus -mthumb -std=c99 '-DHAL_CONFIG=1' '-DDEBUG=1' -Wl,-static -ffunction-sections -Wl,--gc-sections -I./EFM_SDK/emlib/inc/ -I./EFM_SDK/EFM32ZG/Include/ -I./EFM_SDK/CMSIS/Include/ -Og -Wall -fmessage-length=0 --specs=nano.specs -mno-sched-prolog -fno-builtin -ffunction-sections -fdata-sections -MMD -MP

# This is Release Mode with all the size Optimizations, use this after your code
# works!
CFLAGS=-Os -mcpu=cortex-m0plus -mthumb -std=c99 '-DHAL_CONFIG=1' '-DDEBUG=0' -Wl,-static -ffunction-sections -Wl,--gc-sections -I./EFM_SDK/emlib/inc/ -I./EFM_SDK/EFM32ZG/Include/ -I./EFM_SDK/CMSIS/Include/ -Wall -Wextra -pedantic -fmessage-length=0 --specs=nano.specs -mno-sched-prolog -fno-builtin -ffunction-sections -fdata-sections -MMD -MP
ASMFLAGS=-x assembler-with-cpp -DLOOP_ADDR=0x8000
LDFLAGS=-flto -T EFM_SDK/$(MICRO).ld -Xlinker --gc-sections -Xlinker -Map="EFM32ZG108F8.map" -Wl,--start-group -lgcc -lc -lnosys -Wl,--end-group
PROGRAMMER=jlink
OUTFORMAT=bin
FLASH_START=0x0
PROT=SWD # SWD or JTAG

# Add additional C files to the sources below
SOURCES=$(wildcard EFM_SDK/emlib/src/*.c)
#SOURCES+=EFM_SDK/EFM32ZG/Source/GCC/startup_efm32zg.c
SOURCES+=EFM_SDK/EFM32ZG/Source/GCC/startup_efm32zg.S
SOURCES+=EFM_SDK/EFM32ZG/Source/system_efm32zg.c

SOURCES+=source/main.c source/badge.c source/capsense.c source/program.c source/soft_pwm.c source/aes.c
OBJECTS=$(filter %.o, $(patsubst %.S,%.o,$(SOURCES)))
OBJECTS+=$(filter %.o, $(patsubst %.c,%.o,$(SOURCES)))
DEPENDS=$(OBJECTS:.o=.d)

OUTPUT=EFM32ZG108F8


all: $(SOURCES) $(OUTPUT).$(OUTFORMAT)

.PHONY: clean flash erase size jlink reset debug

$(OUTPUT).$(OUTFORMAT): $(OBJECTS)
	$(CC) $(CFLAGS) $(LDFLAGS) -D$(MICRO)=1 $(OBJECTS) -o $(OUTPUT).axf
	$(OBJ) -O ihex $(OUTPUT).axf $(OUTPUT).hex
	$(OBJ) -O binary $(OUTPUT).axf $(OUTPUT).bin
	$(OBJ) -O srec $(OUTPUT).axf $(OUTPUT).s37
	$(SIZE) $(OUTPUT).axf

.c.o:
	$(CC) $(CFLAGS) -D$(MICRO)=1 -c $< -o $@

.S.o:
	$(CC) $(CFLAGS) $(ASMFLAGS) -D$(MICRO)=1 -c $< -o $@

.jlink/flash.jlink:
	cd .jlink; ./flash.py $(MICRO) $(OUTPUT).$(OUTFORMAT) $(FLASH_START)

flash: $(OUTPUT).$(OUTFORMAT) .jlink/flash.jlink
	JLinkExe -device $(MICRO) -speed 4000 -if $(PROT) -CommanderScript .jlink/flash.jlink

erase:
	JLinkExe -device $(MICRO) -speed 4000 -if $(PROT) -CommanderScript .jlink/erase.jlink

size: $(OUTPUT).$(OUTFORMAT)
	$(SIZE) $(OUTPUT).axf

reset:
	JLinkExe -device $(MICRO) -speed 4000 -if $(PROT) -CommanderScript .jlink/reset.jlink

debug:
	JLinkGDBServer -device $(MICRO) -speed 4000 -if $(PROT)

clean:
	rm -f $(OBJECTS)
	rm -f $(DEPENDS)
	rm -f $(OUTPUT).elf
	rm -f $(OUTPUT).$(OUTFORMAT)
	rm -f $(OUTPUT).axf
	rm -f $(OUTPUT).hex
	rm -f $(OUTPUT).s37
	rm -f $(OUTPUT).d
	rm -f $(OUTPUT).map
	rm -f ./.jlink/flash.jlink
