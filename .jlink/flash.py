#!/usr/bin/env python3
import os
import argparse
from string import Template

parser = argparse.ArgumentParser()
parser.add_argument("DEVICE")
parser.add_argument("BINARY")
parser.add_argument("FLASH_START")
args = parser.parse_args()

args_dict = vars(args)

src = None
with open("flash.template",'r') as flash_file:
    src = Template(flash_file.read())
result = src.substitute(args_dict)

with open("flash.jlink",'w') as flash_jlink:
    flash_jlink.write(result)

